
CXX=g++
CXX_FLAGS=-Wall -Wextra -pedantic -fsanitize=address

.PHONY: run test clean

exe: main.cpp
	$(CXX) $(CXX_FLAGS) main.cpp -o exe

run: exe
	./exe

clean:
	rm -f ./exe

test: exe
	for file in test*; do ./exe < $$file; done;

